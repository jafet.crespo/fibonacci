import React from "react";
import "../App.css"
import MostrarFibonacci from "../helpers/MostrarFibonacci";

const RamaFibonacci = ({ value }) => {
  return (
    <div className="rama-principal">
      {value >= 1 && <div className="rama-contenedor">{"F("+ value + ")" + " = " + MostrarFibonacci(value)}</div>}
      {value >= 1 && (
        <div className="ramas-hijas">
          <RamaFibonacci value={value - 1} />
          <RamaFibonacci value={value - 2} />
        </div>
      )}
    </div>
  );
};

export default RamaFibonacci;
