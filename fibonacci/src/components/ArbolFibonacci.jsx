import React, { useState } from "react";
import MostrarFibonacci from "../helpers/MostrarFibonacci";
import RamaFibonacci from "./RamaFibonacci";


const ArbolFibonacci = () => {
  const [input, setInput] = useState(2);

  return (
    <div>
      <h2>Números fibonacci</h2>
      <input type="number" value={input} onChange={(e) => setInput(e.target.value)} />
      <RamaFibonacci value={input} />
    </div>
  );
};

export default ArbolFibonacci;
