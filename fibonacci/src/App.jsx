import React from 'react'
import './App.css'
import ArbolFibonacci from './components/ArbolFibonacci'

function App() {
  return (
    <div className="App">
       <ArbolFibonacci /> 
    </div>
  )
}

export default App
