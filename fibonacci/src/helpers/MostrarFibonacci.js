export default function MostrarFibonacci(size) {
  //RECURSIVO
  /*
  if (size <= 1) return size;
  return MostrarFibonacci(size - 1) + MostrarFibonacci(size - 2);
  */

  /*

  //ITERATIVO
  if (size <= 1) return size;
  let fibo = 1;
  let actual = 1;
  let anterior = 0;
  for (let i = 1; i < size; i++) {
    fibo = actual + anterior;
    anterior = actual;
    actual = fibo;
  }
  return fibo;*/

  //MAP
  let tam = parseInt(size) + 1;
  const resp = Array.from({ length: tam });
  resp.map((_, index) => {
    if (index > 1) {
      resp[index] = resp[index - 1] + resp[index - 2];
    } else {
      resp[index] = index;
    }
  });
  return resp[resp.length - 1];
}
